import Vue from 'vue'
import Modal from '~/components/common/Modal.vue'

const components = { Modal}

Object.keys(components).forEach(key => {
  Vue.component(key, components[key])
})
