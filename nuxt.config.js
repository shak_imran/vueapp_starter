module.exports = {
  /*
  ** Headers of the page
  */
  mode:'spa',

  head: {
    title: 'app',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Admin panel' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [     
    ]   
  },

  css: [
    '~/plugins/metronic/global/plugins/font-awesome/css/font-awesome.min.css',
    '~/plugins/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css',
    '~/plugins/metronic/global/plugins/bootstrap/css/bootstrap.min.css',
    '~/plugins/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
    '~/plugins/metronic/global/plugins/bootstrap-toastr/toastr.min.css',
    '~/plugins/metronic/global/css/components.min.css',
    '~/plugins/metronic/global/css/plugins.min.css',
    '~/plugins/metronic/layouts/layout4/css/layout.min.css',
    '~/plugins/metronic/layouts/layout4/css/themes/default.min.css',
    '~/plugins/metronic/layouts/layout4/css/custom.min.css'    
  ],
  

  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    vendor: [],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }   
  },


  plugins: [
    { src: '~/plugins/metronic/global/plugins/jquery.min.js', ssr: false },
    { src: '~/plugins/metronic/global/plugins/bootstrap/js/bootstrap.min.js', ssr: false },
    { src: '~/plugins/metronic/global/plugins/js.cookie.min.js', ssr: false },
    { src: '~/plugins/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js', ssr: false },
    { src: '~/plugins/metronic/global/plugins/jquery.blockui.min.js', ssr: false },
    { src: '~/plugins/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js', ssr: false },
    { src: '~/plugins/metronic/global/plugins/bootstrap-toastr/toastr.min.js', ssr: false },
    { src: '~/plugins/metronic/global/scripts/app.js', ssr: false },
    { src: '~/plugins/helper', ssr: false },
    { src: '~/plugins/chelper', ssr: false },    
    { src: '~/plugins/vue-sweetalert', ssr: false },
    { src: '~/plugins/vee-validate', ssr: true },
    { src: '~/plugins/masked-input', ssr: true },
    { src: '~/plugins/vue-tables-2', ssr: false },
    { src: '~/plugins/vue-toastr', ssr: false },    
    { src: '~/plugins/common-components'}
  ]
}
