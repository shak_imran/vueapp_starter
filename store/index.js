import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
Vue.use(Vuex)


const createStore = () => {
  return new Vuex.Store({
    state: {
      isAuthenticated: false,
      token:'',
      user: {       
      },
      menuItems: [        
      ]
    },
    mutations: {
      SetIdentity(state, response) {
        state.user = response.user
        state.token = response.token
        state.isAuthenticated = true
      }
    },

    plugins: [(new VuexPersistence({
      storage: window.localStorage
    })).plugin]
  })
}

export default createStore
