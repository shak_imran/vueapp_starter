import 'es6-promise/auto'
import Vue from 'vue'
import Meta from 'vue-meta'
import { createRouter } from './router.js'
import NoSSR from './components/no-ssr.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtLink from './components/nuxt-link.js'
import NuxtError from './components/nuxt-error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData } from './utils'
import { createStore } from './store.js'

/* Plugins */
import nuxt_plugin_jquerymin_5407e7b8 from 'nuxt_plugin_jquerymin_5407e7b8' // Source: ..\\plugins\\metronic\\global\\plugins\\jquery.min.js (ssr: false)
import nuxt_plugin_bootstrapmin_33c8d739 from 'nuxt_plugin_bootstrapmin_33c8d739' // Source: ..\\plugins\\metronic\\global\\plugins\\bootstrap\\js\\bootstrap.min.js (ssr: false)
import nuxt_plugin_jscookiemin_5eff227b from 'nuxt_plugin_jscookiemin_5eff227b' // Source: ..\\plugins\\metronic\\global\\plugins\\js.cookie.min.js (ssr: false)
import nuxt_plugin_jqueryslimscrollmin_fac76cc0 from 'nuxt_plugin_jqueryslimscrollmin_fac76cc0' // Source: ..\\plugins\\metronic\\global\\plugins\\jquery-slimscroll\\jquery.slimscroll.min.js (ssr: false)
import nuxt_plugin_jqueryblockuimin_1462afd1 from 'nuxt_plugin_jqueryblockuimin_1462afd1' // Source: ..\\plugins\\metronic\\global\\plugins\\jquery.blockui.min.js (ssr: false)
import nuxt_plugin_bootstrapswitchmin_16f5ea76 from 'nuxt_plugin_bootstrapswitchmin_16f5ea76' // Source: ..\\plugins\\metronic\\global\\plugins\\bootstrap-switch\\js\\bootstrap-switch.min.js (ssr: false)
import nuxt_plugin_toastrmin_531a3814 from 'nuxt_plugin_toastrmin_531a3814' // Source: ..\\plugins\\metronic\\global\\plugins\\bootstrap-toastr\\toastr.min.js (ssr: false)
import nuxt_plugin_app_5b9bb43f from 'nuxt_plugin_app_5b9bb43f' // Source: ..\\plugins\\metronic\\global\\scripts\\app.js (ssr: false)
import nuxt_plugin_helper_619f8ab8 from 'nuxt_plugin_helper_619f8ab8' // Source: ..\\plugins\\helper (ssr: false)
import nuxt_plugin_chelper_13122d9b from 'nuxt_plugin_chelper_13122d9b' // Source: ..\\plugins\\chelper (ssr: false)
import nuxt_plugin_vuesweetalert_184a9809 from 'nuxt_plugin_vuesweetalert_184a9809' // Source: ..\\plugins\\vue-sweetalert (ssr: false)
import nuxt_plugin_veevalidate_6e5ad03a from 'nuxt_plugin_veevalidate_6e5ad03a' // Source: ..\\plugins\\vee-validate
import nuxt_plugin_maskedinput_7c4b8b04 from 'nuxt_plugin_maskedinput_7c4b8b04' // Source: ..\\plugins\\masked-input
import nuxt_plugin_vuetables2_3675e9a7 from 'nuxt_plugin_vuetables2_3675e9a7' // Source: ..\\plugins\\vue-tables-2 (ssr: false)
import nuxt_plugin_vuetoastr_07f02570 from 'nuxt_plugin_vuetoastr_07f02570' // Source: ..\\plugins\\vue-toastr (ssr: false)
import nuxt_plugin_commoncomponents_34987382 from 'nuxt_plugin_commoncomponents_34987382' // Source: ..\\plugins\\common-components


// Component: <no-ssr>
Vue.component(NoSSR.name, NoSSR)

// Component: <nuxt-child>
Vue.component(NuxtChild.name, NuxtChild)

// Component: <nuxt-link>
Vue.component(NuxtLink.name, NuxtLink)

// Component: <nuxt>`
Vue.component(Nuxt.name, Nuxt)

// vue-meta configuration
Vue.use(Meta, {
  keyName: 'head', // the component option name that vue-meta looks for meta info on.
  attribute: 'data-n-head', // the attribute name vue-meta adds to the tags it observes
  ssrAttribute: 'data-n-head-ssr', // the attribute name that lets vue-meta know that meta info has already been server-rendered
  tagIDKeyName: 'hid' // the property name that vue-meta uses to determine whether to overwrite or append a tag
})

const defaultTransition = {"name":"page","mode":"out-in","appear":true,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

async function createApp (ssrContext) {
  const router = createRouter()

  
  const store = createStore()
  // Add this.$router into store actions/mutations
  store.$router = router
  

  // Create Root instance
  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    router,
    store,
    nuxt: {
      defaultTransition,
      transitions: [ defaultTransition ],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [ transitions ]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },
      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = !!err
        if (typeof err === 'string') err = { statusCode: 500, message: err }
        const nuxt = this.nuxt || this.$options.nuxt
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in lib/server.js
        if (ssrContext) ssrContext.nuxt.error = err
        return err
      }
    },
    ...App
  }
  
  // Make app available into store via this.app
  store.app = app
  
  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    route,
    next,
    error: app.nuxt.error.bind(app),
    store,
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined
  })

  const inject = function (key, value) {
    if (!key) throw new Error('inject(key, value) has no key provided')
    if (!value) throw new Error('inject(key, value) has no value provided')
    key = '$' + key
    // Add into app
    app[key] = value
    
    // Add into store
    store[key] = app[key]
    
    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) return
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Vue.prototype.hasOwnProperty(key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  
  if (process.browser) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }
  

  // Plugin execution
  
  if (typeof nuxt_plugin_veevalidate_6e5ad03a === 'function') await nuxt_plugin_veevalidate_6e5ad03a(app.context, inject)
  if (typeof nuxt_plugin_maskedinput_7c4b8b04 === 'function') await nuxt_plugin_maskedinput_7c4b8b04(app.context, inject)
  if (typeof nuxt_plugin_commoncomponents_34987382 === 'function') await nuxt_plugin_commoncomponents_34987382(app.context, inject)
  
  if (process.browser) { 
    if (typeof nuxt_plugin_jquerymin_5407e7b8 === 'function') await nuxt_plugin_jquerymin_5407e7b8(app.context, inject)
    if (typeof nuxt_plugin_bootstrapmin_33c8d739 === 'function') await nuxt_plugin_bootstrapmin_33c8d739(app.context, inject)
    if (typeof nuxt_plugin_jscookiemin_5eff227b === 'function') await nuxt_plugin_jscookiemin_5eff227b(app.context, inject)
    if (typeof nuxt_plugin_jqueryslimscrollmin_fac76cc0 === 'function') await nuxt_plugin_jqueryslimscrollmin_fac76cc0(app.context, inject)
    if (typeof nuxt_plugin_jqueryblockuimin_1462afd1 === 'function') await nuxt_plugin_jqueryblockuimin_1462afd1(app.context, inject)
    if (typeof nuxt_plugin_bootstrapswitchmin_16f5ea76 === 'function') await nuxt_plugin_bootstrapswitchmin_16f5ea76(app.context, inject)
    if (typeof nuxt_plugin_toastrmin_531a3814 === 'function') await nuxt_plugin_toastrmin_531a3814(app.context, inject)
    if (typeof nuxt_plugin_app_5b9bb43f === 'function') await nuxt_plugin_app_5b9bb43f(app.context, inject)
    if (typeof nuxt_plugin_helper_619f8ab8 === 'function') await nuxt_plugin_helper_619f8ab8(app.context, inject)
    if (typeof nuxt_plugin_chelper_13122d9b === 'function') await nuxt_plugin_chelper_13122d9b(app.context, inject)
    if (typeof nuxt_plugin_vuesweetalert_184a9809 === 'function') await nuxt_plugin_vuesweetalert_184a9809(app.context, inject)
    if (typeof nuxt_plugin_vuetables2_3675e9a7 === 'function') await nuxt_plugin_vuetables2_3675e9a7(app.context, inject)
    if (typeof nuxt_plugin_vuetoastr_07f02570 === 'function') await nuxt_plugin_vuetoastr_07f02570(app.context, inject)
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, () => {
        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from, next) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    app,
    router,
    store
  }
}

export { createApp, NuxtError }
