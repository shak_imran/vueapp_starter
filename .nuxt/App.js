import Vue from 'vue'
import NuxtLoading from './components/nuxt-loading.vue'

import '..\\plugins\\metronic\\global\\plugins\\font-awesome\\css\\font-awesome.min.css'

import '..\\plugins\\metronic\\global\\plugins\\simple-line-icons\\simple-line-icons.min.css'

import '..\\plugins\\metronic\\global\\plugins\\bootstrap\\css\\bootstrap.min.css'

import '..\\plugins\\metronic\\global\\plugins\\bootstrap-switch\\css\\bootstrap-switch.min.css'

import '..\\plugins\\metronic\\global\\plugins\\bootstrap-toastr\\toastr.min.css'

import '..\\plugins\\metronic\\global\\css\\components.min.css'

import '..\\plugins\\metronic\\global\\css\\plugins.min.css'

import '..\\plugins\\metronic\\layouts\\layout4\\css\\layout.min.css'

import '..\\plugins\\metronic\\layouts\\layout4\\css\\themes\\default.min.css'

import '..\\plugins\\metronic\\layouts\\layout4\\css\\custom.min.css'


let layouts = {

  "_default": () => import('..\\layouts\\default.vue'  /* webpackChunkName: "layouts_default" */).then(m => m.default || m),

  "_empty": () => import('..\\layouts\\empty.vue'  /* webpackChunkName: "layouts_empty" */).then(m => m.default || m)

}

let resolvedLayouts = {}

export default {
  head: {"title":"app","meta":[{"charset":"utf-8"},{"name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"description","name":"description","content":"Admin panel"}],"link":[{"rel":"icon","type":"image\u002Fx-icon","href":"\u002Ffavicon.ico"}],"script":[],"style":[]},
  render(h, props) {
    const loadingEl = h('nuxt-loading', { ref: 'loading' })
    const layoutEl = h(this.layout || 'nuxt')
    const templateEl = h('div', {
      domProps: {
        id: '__layout'
      },
      key: this.layoutName
    }, [ layoutEl ])

    const transitionEl = h('transition', {
      props: {
        name: 'layout',
        mode: 'out-in'
      }
    }, [ templateEl ])

    return h('div',{
      domProps: {
        id: '__nuxt'
      }
    }, [
      loadingEl,
      transitionEl
    ])
  },
  data: () => ({
    layout: null,
    layoutName: ''
  }),
  beforeCreate () {
    Vue.util.defineReactive(this, 'nuxt', this.$options.nuxt)
  },
  created () {
    // Add this.$nuxt in child instances
    Vue.prototype.$nuxt = this
    // add to window so we can listen when ready
    if (typeof window !== 'undefined') {
      window.$nuxt = this
    }
    // Add $nuxt.error()
    this.error = this.nuxt.error
  },
  
  mounted () {
    this.$loading = this.$refs.loading
  },
  watch: {
    'nuxt.err': 'errorChanged'
  },
  
  methods: {
    
    errorChanged () {
      if (this.nuxt.err && this.$loading) {
        if (this.$loading.fail) this.$loading.fail()
        if (this.$loading.finish) this.$loading.finish()
      }
    },
    
    setLayout (layout) {
      if (!layout || !resolvedLayouts['_' + layout]) layout = 'default'
      this.layoutName = layout
      let _layout = '_' + layout
      this.layout = resolvedLayouts[_layout]
      return this.layout
    },
    loadLayout (layout) {
      if (!layout || !(layouts['_' + layout] || resolvedLayouts['_' + layout])) layout = 'default'
      let _layout = '_' + layout
      if (resolvedLayouts[_layout]) {
        return Promise.resolve(resolvedLayouts[_layout])
      }
      return layouts[_layout]()
      .then((Component) => {
        resolvedLayouts[_layout] = Component
        delete layouts[_layout]
        return resolvedLayouts[_layout]
      })
      .catch((e) => {
        if (this.$nuxt) {
          return this.$nuxt.error({ statusCode: 500, message: e.message })
        }
      })
    }
  },
  components: {
    NuxtLoading
  }
}

