import CHelperCore from '~/helpers/chelper-core.js'

export default {
  install: (Vue, options) => {
    Vue.prototype.$CHelpers = {

      Test() {
        return CHelperCore.Test()
      },

      SetUser(user) {
        CHelperCore.SetUser(user)
      },

      ClearUser() {
        CHelperCore.ClearUser()
      },

      GetUser() {
        return CHelperCore.GetUser()
      }
    }
  }
};

